import asyncio

import requests
from aiogram import Bot, Dispatcher, executor, types
from googletrans import Translator  # pip install googletrans==3.1.0a0

# from dotevn.main import load_dotevn
# from  sys import  exit
# #
# load_dotevn()

trans=Translator()

class Cats_Foto():
    url1= f'https://api.thecatapi.com/v1/images/search?live_p4pS7LVPQtSlWiKHZ3oCc0qESRTu2KRH3qvaKTMtjfZsp1sUnY2S2l7ZgxZzHqN6'

    def __init__(self,url:str):
        self.url = self.url1

    def foto(self):
        response = requests.get(f'{self.url}/random/url')
        self.data = response.json()[0]
        foto = self.data['url']
        return foto

class Cats_Fact():
    url2=f'https://catfact.ninja/fact'

    def __init__(self,url:str):
        self.url = self.url2

    def fact(self):
        response = requests.get(f'{self.url}')
        self.data = response.json()
        fact_text = self.data['fact']
        # print(fact_text)
        fact_ru = trans.translate(fact_text, dest='ru').text
        # print(fact_ru)
        return fact_ru

class Dog_Foto():
    url3= f'https://random.dog/woof.json'

    def __init__(self,url:str):
        self.url = self.url3

    def foto(self):
        response = requests.get(f'{self.url}')
        # print("фото response ", response)
        self.data = response.json()
        # print("фото self.data ", self.data)
        foto = self.data['url']
        # print("фото foto", foto)
        return foto

class Dog_Fact():
    url4=f'https://dogapi.dog/api/v2/facts?limit=1'

    def __init__(self,url:str):
        self.url = self.url4

    def fact(self):
        response = requests.get(f'{self.url}')
        print("факт response text ", response.text)
        fact = response.json()['data'][0]['attributes']
        self.fact_text = trans.translate(fact['body'], dest='ru').text
        return self.fact_text



loop=asyncio.get_event_loop()
bot = Bot("5992325332:AAHEPQjkQNIWvfETtcxtuWyn0COV4OKQXUg", parse_mode='HTML')
dp = Dispatcher(bot=bot, loop=loop)
cats1 = Cats_Foto(f'https://api.thecatapi.com/v1/images/search?live_p4pS7LVPQtSlWiKHZ3oCc0qESRTu2KRH3qvaKTMtjfZsp1sUnY2S2l7ZgxZzHqN6')
cats2 = Cats_Fact(f'https://catfact.ninja/fact')
dog1 = Dog_Foto(f'https://random.dog/woof.json')
dog2 = Dog_Fact(f'https://dogapi.dog/api/v2/facts?limit=1')


@dp.message_handler(commands=['start'])

async def start_handler(message: types.Message):
    keyword = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton("🐱 Факт о кошках")
    btn2 = types.KeyboardButton("🐶 Факт о собаках")
    btn3 = types.KeyboardButton("MARINA")
    keyword.add(btn1, btn2, btn3)

    await message.answer("Приветствую тебя {0.first_name}, любознательный человек! Я - твой верный помощник в мире пушистых и лапатых существ. "
                          "Готов удивить тебя захватывающими фактами о четвероногих друзьях, бывшими божествами в древности и настоящими паладинами нынешних дней. "
                          "Давай погрузимся в замечательный мир кошек и собак, который окружает нас! Okay".format(
                         message.from_user), reply_markup=keyword)


@dp.message_handler(lambda message: message.text == "🐱 Факт о кошках")
async def cats_handler(message: types.Message):
    await (bot.send_photo(message.chat.id, photo=cats1.foto(), caption=cats2.fact()))
    bot.polling(none_stop=True)

@dp.message_handler(lambda message: message.text == "🐶 Факт о собаках")
async def dog_handler(message: types.Message):
    await (bot.send_photo(message.chat.id, photo=dog1.foto(), caption=dog2.fact()))
    bot.polling(none_stop=True)

@dp.message_handler(lambda message: message.text == "MARINA")
async def cats_handler(message: types.Message):
    await (bot.send_photo(message.chat.id, photo=cats1.foto(), caption=cats2.fact()))

# @dp.message_handler(lambda message: message.text == "❓ Задать вопрос")
# async def cats_handler(message: types.Message):
#     def func(message):
#         if (message.text == "❓ Задать вопрос"):
#             keyword = types.ReplyKeyboardMarkup(resize_keyboard=True)
#             btn1 = types.KeyboardButton("🆘 Помощь")
#             btn2 = types.KeyboardButton("Что я могу?")
#             back = types.KeyboardButton("🔙 Вернуться в главное меню")
#             keyword.add(btn1, btn2, back)
#             bot.send_message(message.chat.id, text="Выбери нужную кнопку ⬇️", reply_markup=keyword)
#         elif (message.text == "🆘 Помощь"):
#             bot.send_message(message.chat.id, text="Мы всегда рады тебе помочь разобраться с использованием данного бота))")
#         elif (message.text == "Что я могу?"):
#             bot.send_message(message.chat.id, text="Рассказать о фактах собак или кошек с милыми картинками...")
#         elif (message.text == "🔙 Вернуться в главное меню"):
#             keyword = types.ReplyKeyboardMarkup(resize_keyboard=True)
#             button1 = types.KeyboardButton("🐱 Факт о кошках")
#             button2 = types.KeyboardButton("🐶 Факт о собаках")
#             button3 = types.KeyboardButton("MARINA")
#             button4 = types.KeyboardButton("❓ Задать вопрос")
#             keyword.add(button1, button2, button3, button4)
#             bot.send_message(message.chat.id, text="Вы вернулись в главное меню", reply_markup=keyword)
#         else:
#             bot.send_message(message.chat.id, text="На такую комманду я не запрограммировал..")





if __name__ == '__main__':

    executor.start_polling(dp)